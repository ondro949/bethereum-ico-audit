pragma solidity ^0.4.18;

import '../installed_contracts/zeppelin/contracts/crowdsale/FinalizableCrowdsale.sol';

//NOTES:

contract BTHRMTokenSale is FinalizableCrowdsale {
    using SafeMath for uint256; 
    
    // Define sale
    uint public constant RATE = 21500;
    uint public constant TOKEN_SALE_LIMIT = 20000 * 1000000000000000000;

    uint256 public constant TOKENS_FOR_OPERATIONS = 400000000*(10**18);
    uint256 public constant TOKENS_FOR_SALE = 600000000*(10**18);

    uint public constant TOKENS_FOR_PRESALE = 387000000*(1 ether / 1 wei);

    uint public constant FRST_CRWDSALE_RATIO = TOKENS_FOR_PRESALE + 100620000*(1 ether / 1 wei);//30% bonus
    uint public constant SCND_CRWDSALE_RATIO = FRST_CRWDSALE_RATIO + 89010000*(1 ether / 1 wei);//15% bonus

    enum Phase {
        Created,//Inital phase after deploy
        PresaleRunning, //Presale phase
        Paused, //Pause phase between pre-sale and main token sale or emergency pause function
        ICORunning, //Crowdsale phase
        FinishingICO //Final phase when crowdsale is closed and time is up
    }

    Phase public currentPhase = Phase.Created;
    
    event LogPhaseSwitch(Phase phase);

    // Constructor
    function BTHRMTokenSale(uint256 _end, address _wallet)
             FinalizableCrowdsale()
             Crowdsale(_end, _wallet) {
    }

    /// @dev Lets buy you some tokens.
    function buyTokens(address _buyer) public payable {
        // Available only if presale or crowdsale is running.
        require((currentPhase == Phase.PresaleRunning) || (currentPhase == Phase.ICORunning));
        require(_buyer != address(0));
        require(msg.value > 0);
        require(validPurchase());

        uint256 weiAmount = msg.value;
        weiRaised = weiRaised.add(weiAmount);

        uint newTokens = msg.value * RATE;

        require(weiRaised <= TOKEN_SALE_LIMIT);

        newTokens = addBonusTokens(token.totalSupply(), newTokens);

        require(newTokens + weiRaised <= TOKENS_FOR_SALE);

        token.mint(_buyer, newTokens);
        TokenPurchase(msg.sender, _buyer, weiAmount, newTokens);

        forwardFunds();
    }

    // @dev Adds bonus tokens by token supply bought by user
    // @param _totalSupply total supply of token bought during pre-sale/crowdsale
    // @param _newTokens tokens currently bought by user
    function addBonusTokens(uint256 _totalSupply, uint256 _newTokens) internal view returns (uint256) {
        if ((currentPhase == Phase.PresaleRunning) && _totalSupply + _newTokens <= TOKENS_FOR_PRESALE) return _newTokens.add(_newTokens * 50/100); //first 50% bonus in pre-sale phase
        if (_totalSupply + _newTokens <= FRST_CRWDSALE_RATIO) return _newTokens.add(_newTokens * 30/100); //first 30% bonus
        if (_totalSupply + _newTokens <= SCND_CRWDSALE_RATIO) return _newTokens.add(_newTokens * 15/100); //second 15% bonus
        if (_totalSupply + _newTokens > SCND_CRWDSALE_RATIO) return _newTokens;  //third get 0% bonus
    }

    function validPurchase() internal view returns (bool) {
        bool withinPeriod = now <= endTime;
        bool nonZeroPurchase = msg.value != 0;
        bool isRunning = ((currentPhase == Phase.ICORunning) || (currentPhase == Phase.PresaleRunning));
        return withinPeriod && nonZeroPurchase && isRunning;
    }

    function setSalePhase(Phase _nextPhase) public onlyOwner {
        bool canSwitchPhase
        =  (currentPhase == Phase.Created && _nextPhase == Phase.PresaleRunning)
        || (currentPhase == Phase.PresaleRunning && _nextPhase == Phase.Paused)
        || ((currentPhase == Phase.PresaleRunning || currentPhase == Phase.Paused)
        && _nextPhase == Phase.ICORunning)
        || (currentPhase == Phase.ICORunning && _nextPhase == Phase.Paused)
        || (currentPhase == Phase.Paused && _nextPhase == Phase.FinishingICO)
        || (currentPhase == Phase.ICORunning && _nextPhase == Phase.FinishingICO);

        require(canSwitchPhase);
        currentPhase = _nextPhase;
        LogPhaseSwitch(_nextPhase);
    }

    // Finalize
    function finalization() internal {
        uint256 toMint = TOKENS_FOR_OPERATIONS;
        token.mint(wallet, toMint);
        token.finishMinting();
        token.transferOwnership(wallet);
    }
}
