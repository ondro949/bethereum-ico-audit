pragma solidity ^0.4.18;

import '../installed_contracts/zeppelin/contracts/token/MintableToken.sol';
import '../installed_contracts/zeppelin/contracts/token/PausableToken.sol';

contract BethereumToken is MintableToken, PausableToken {
    string public constant name = "Bethereum Token";
    string public constant symbol = "BTHR";
    uint256 public constant decimals = 18;

    function BethereumToken(){
        pause();
    }

}